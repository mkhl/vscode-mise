import cp from 'child_process'
import { promisify } from 'util'

import { LogOutputChannel, WorkspaceFolder } from 'vscode'

import config from './config'

const execFile = promisify(cp.execFile)

export interface Task {
	name: string
}

export default class Mise {
	constructor(private output: LogOutputChannel) {}

	async tasks(workspace?: WorkspaceFolder): Promise<Task[]> {
		const output = await this.mise(['task', 'ls', '--json'], workspace)
		const tasks = JSON.parse(output) as Task[]
		return tasks
	}

	async isAvailable(workspace?: WorkspaceFolder): Promise<boolean> {
		try {
			await this.mise(['version'], workspace)
			return true
		} catch (_) {
			return false
		}
	}

	async isExperimental(workspace?: WorkspaceFolder): Promise<boolean> {
		try {
			const output = await this.mise(['settings', 'get', 'experimental'], workspace)
			return output.trim() === 'true'
		} catch (_) {
			return false
		}
	}

	async enableExperimental(workspace?: WorkspaceFolder): Promise<void> {
		await this.mise(['settings', 'set', 'experimental', 'true'], workspace)
	}

	private async mise(
		args: string[],
		workspace?: WorkspaceFolder,
		env: NodeJS.ProcessEnv = {},
	): Promise<string> {
		const cwd = workspace?.uri.fsPath ?? process.cwd()
		const cmd = config.resolve(workspace).executablePath
		const options: cp.ExecOptionsWithStringEncoding = {
			encoding: 'utf8',
			cwd,
			env: {
				...process.env,
				['TERM']: 'dumb',
				...env,
			},
		}
		try {
			const { stdout, stderr } = await execFile(cmd, args, options)
			if (stderr) {
				this.output.info(stderr)
			}
			return stdout
		} catch (e) {
			if (typeof e === 'string' || e instanceof Error) {
				this.output.error(e)
			}
			throw e
		}
	}
}
