import vscode, {
	Disposable,
	ProcessExecution,
	ProviderResult,
	RelativePattern,
	Task,
	TaskDefinition,
	TaskProvider,
	WorkspaceFolder,
} from 'vscode'

import config from './config'
import Mise, * as mise from './mise'

export class MiseTaskProvider implements Disposable, TaskProvider {
	static readonly Type = 'mise'

	private workspaces = new Map<string, MiseTaskWorkspace>()
	private disposable: Disposable

	constructor(private mise: Mise) {
		const folders = vscode.workspace.workspaceFolders ?? []
		for (const folder of folders) {
			this.addWorkspace(folder)
		}
		this.disposable = Disposable.from(
			vscode.workspace.onDidChangeWorkspaceFolders((e) => {
				for (const folder of e.added) {
					this.addWorkspace(folder)
				}
				for (const folder of e.removed) {
					this.removeWorkspace(folder)
				}
			}),
		)
	}

	private addWorkspace(folder: WorkspaceFolder) {
		const key = folder.uri.fsPath
		if (!key) return
		if (this.workspaces.has(key)) return
		this.workspaces.set(key, new MiseTaskWorkspace(folder, this.mise))
	}

	private removeWorkspace(folder: WorkspaceFolder) {
		const key = folder.uri.fsPath
		this.workspaces.get(key)?.dispose()
		this.workspaces.delete(key)
	}

	dispose() {
		this.disposable.dispose()
		for (const folder of this.workspaces.values()) {
			folder.dispose()
		}
	}

	async provideTasks(): Promise<Task[]> {
		const workspaces = Array.from(this.workspaces.values())
		const promises = workspaces.map((workspace) => workspace.provideTasks())
		const tasks = await Promise.all(promises)
		return tasks.flat()
	}

	resolveTask(task: Task): ProviderResult<Task> {
		if (!isMiseTaskDefinition(task.definition) || typeof task.scope !== 'object') return
		const command = config.resolve(task.scope).executablePath
		task.execution = execute(command, task)
		return task
	}
}

class MiseTaskWorkspace implements Disposable {
	private disposable: Disposable
	private loading?: Thenable<Task[]>

	constructor(
		private workspace: WorkspaceFolder,
		private mise: Mise,
	) {
		this.disposable = Disposable.from(
			this.watchConfiguration(workspace),
			this.watchFileSystem(workspace, '.mise.toml'),
			this.watchFileSystem(workspace, '.mise/tasks/*'),
		)
	}

	private watchConfiguration(workspace: WorkspaceFolder) {
		return vscode.workspace.onDidChangeConfiguration((e) => {
			if (e.affectsConfiguration(config.section, workspace)) {
				this.reload()
			}
		})
	}

	private watchFileSystem(workspace: WorkspaceFolder, pattern: string): Disposable {
		const relative = new RelativePattern(workspace, pattern)
		const watcher = vscode.workspace.createFileSystemWatcher(relative)
		watcher.onDidChange(() => this.reload())
		watcher.onDidCreate(() => this.reload())
		watcher.onDidDelete(() => this.reload())
		return watcher
	}

	private reload() {
		this.loading = undefined
	}

	dispose() {
		this.disposable.dispose()
	}

	async provideTasks(): Promise<Task[]> {
		if (!this.workspace.uri.fsPath) return []
		if (!this.loading) {
			this.loading = this.loadTasks()
		}
		return this.loading
	}

	private async loadTasks(): Promise<Task[]> {
		const tasks = await this.mise.tasks(this.workspace)
		const command = config.resolve(this.workspace).executablePath
		const type = MiseTaskProvider.Type
		return tasks.map((task) => {
			const definition = { type, ...task }
			const execution = execute(command, task)
			return new Task(definition, this.workspace, task.name, config.section, execution)
		})
	}
}

function execute(command: string, task: mise.Task): ProcessExecution {
	return new ProcessExecution(command, ['task', 'run', task.name])
}

interface MiseTaskDefinition extends mise.Task, TaskDefinition {}

function isMiseTaskDefinition(def: TaskDefinition): def is MiseTaskDefinition {
	return def.type === MiseTaskProvider.Type && 'name' in def
}
