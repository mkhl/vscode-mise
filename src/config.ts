import os from 'os'
import vscode from 'vscode'

export const section = 'mise'

const defaults = {
	executablePath: 'mise',
}

export function resolve(scope?: vscode.WorkspaceFolder): typeof defaults {
	const configuration = vscode.workspace.getConfiguration(section, scope)
	const substitutions = createSubstitutions(scope)
	return Object.fromEntries(
		Object.entries(defaults).map(([key, defaultValue]) => {
			const value = configuration.get(key)
			if (value === undefined) return [key, defaultValue]
			if (typeof value !== 'string') return [key, value]
			return [key, replace(substitutions, value)]
		}),
	) as typeof defaults
}

function createSubstitutions(
	workspaceFolder?: vscode.WorkspaceFolder,
): Map<string, string> {
	const substitutions = new Map<string, string>()
	substitutions.set('userHome', os.homedir())
	substitutions.set('cwd', process.cwd())
	if (workspaceFolder) {
		substitutions.set('workspaceFolder', workspaceFolder.uri.fsPath)
	}
	const workspaceFolders = vscode.workspace.workspaceFolders ?? []
	for (const workspace of workspaceFolders) {
		substitutions.set(`workspaceFolder:${workspace.name}`, workspace.uri.fsPath)
	}
	for (const [key, val] of Object.entries(process.env)) {
		if (val) {
			substitutions.set(`env:${key}`, val)
		}
	}
	return substitutions
}

function replace(subs: Map<string, string>, str: string): string {
	for (const [key, val] of subs) {
		str = str.replace(`\${${key}}`, val)
	}
	return str
}

export async function open(key: keyof typeof defaults) {
	return await vscode.commands.executeCommand(
		'workbench.action.openSettings',
		`${section}.${key}`,
	)
}

export default { section, resolve, open }
