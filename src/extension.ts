import vscode, { WorkspaceFolder } from 'vscode'

import config from './config'
import Mise from './mise'
import { MiseTaskProvider } from './taskProvider'

class Extension {
	private mise: Mise
	private output: vscode.LogOutputChannel

	constructor(private context: vscode.ExtensionContext) {
		this.output = vscode.window.createOutputChannel('mise-en-place', { log: true })
		context.subscriptions.push(this.output)
		this.mise = new Mise(this.output)
	}

	async activate() {
		const workspace = vscode.workspace.workspaceFolders?.[0]
		if (!(await this.checkMise(workspace))) return
		if (await this.enableTasks(workspace)) {
			this.context.subscriptions.push(
				vscode.tasks.registerTaskProvider(
					MiseTaskProvider.Type,
					new MiseTaskProvider(this.mise),
				),
			)
		}
	}

	async checkMise(workspace?: WorkspaceFolder): Promise<boolean> {
		if (await this.mise.isAvailable(workspace)) return true
		const options = ['Configure', 'Help', 'Ignore']
		const choice = await vscode.window.showWarningMessage(
			'Could not run mise, is it installed?',
			...options,
		)
		switch (choice) {
			case 'Configure':
				await config.open('executablePath')
				return true
			case 'Help':
				await vscode.commands.executeCommand(
					'vscode.open',
					'https://mise.jdx.dev/getting-started.html#quickstart',
				)
				return true
		}
		return false
	}

	async enableTasks(workspace?: WorkspaceFolder): Promise<boolean> {
		if (await this.mise.isExperimental(workspace)) return true
		const options = ['Enable', 'Ignore']
		const choice = await vscode.window.showWarningMessage(
			'Could not get mise tasks, are experimental features enabled?',
			...options,
		)
		switch (choice) {
			case 'Enable':
				await this.mise.enableExperimental(workspace)
				return true
		}
		return false
	}
}

export async function activate(context: vscode.ExtensionContext) {
	const extension = new Extension(context)
	await extension.activate()
}

export function deactivate() {
	// nothing
}
