# mise-en-place for Visual Studio Code

[mise] (pronounced "meez") or [mise-en-place]
is a development environment setup tool.
The name refers to a French culinary phrase
that roughly translates to "setup" or "put in place".
The idea is that before one begins cooking,
they should have all their utensils and ingredients
ready to go in their place.

mise does the same for your projects.
Using its `.mise.toml` config file,
you'll have a consistent way to setup and interact with your projects
no matter what language they're written in.

This extension adds mise support to Visual Studio Code by:

* being a [Task Provider][vscode-task-provider] for mise tasks

It does not (yet) support:

* installing or managing dev tools and runtimes

* loading environment variables for the workspace root


## Features

Run tasks with the "Tasks: Run Task" command.


## Requirements

This extension requires [mise] to be installed.

mise tasks are currently experimental
so for the task provider to work
you'll need to enable experimental features with
`mise settings set experimental true`.


## Extension Settings

TODO


## Known Issues

TODO


## Acknowledgements

The logo is [the mise logo][mise-logo].

[mise]: https://mise.jdx.dev/
[mise-en-place]: https://mise.jdx.dev/
[mise-logo]: https://github.com/jdx/mise
[vscode-task-provider]: https://code.visualstudio.com/api/extension-guides/task-provider
